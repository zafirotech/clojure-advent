(ns clojure-advent.level1-elevator
  (:require [clojure.java.io :as io]))

(def elevator-seq (slurp (io/resource "level1-input.txt")))

(defn paradd
  "add parentesis to number '(' means 1 up, ')' one down"
  [numb par-str]
  (if (= par-str \()
    (inc numb)
    (dec numb))
  )

(defn level-task
  "Main entry exec for the level"
  []
  ;; Task 1
  (println "-- Level1 --")
  (println (str "Part1: " (reduce paradd 0 (seq elevator-seq))))
  ;; Task 2
  (loop [index 0 action-seq (seq elevator-seq) floor 0]
    (if (< floor 0)
      (println (str "Part2: " index))
      (let [[first & remaining] action-seq]
        (recur (inc index) remaining (paradd floor first))))
    )
  (println)
  )