(ns clojure-advent.level3-xmas-gifts
  (:require [clojure.java.io :as io]))

(def santa-directions-seq (slurp (io/resource "level3-input.txt")))

(defn coord-transf
  "Tranformation coordinate vector"
  [new-direction]
  (case new-direction
    \> {:x 1 :y 0}
    \< {:x -1 :y 0}
    \^ {:x 0 :y 1}
    \v {:x 0 :y -1}
    ))

(defn coord-add
  "Add two coordinates"
  [coord1 coord2]
  {:x (+ (:x coord1) (:x coord2)) :y (+ (:y coord1) (:y coord2))})

(defn santa-walker
  "Walks over the houses returning current location and keeping track of alls"
  [board-track new-direction]
  (let [new-coord (coord-add (:current-location board-track) (coord-transf new-direction))]
    {:current-location new-coord :all-locations (conj (:all-locations board-track) new-coord)}
    ))

(def board-track (reduce santa-walker {:current-location {:x 0 :y 0} :all-locations (set [{:x 0 :y 0}])} (seq santa-directions-seq)))

(def board-track-santa-only (reduce santa-walker {:current-location {:x 0 :y 0} :all-locations (set [{:x 0 :y 0}])} (take-nth 2 (seq santa-directions-seq))))
(def board-track-robot-only (reduce santa-walker {:current-location {:x 0 :y 0} :all-locations (set [{:x 0 :y 0}])} (take-nth 2 (rest (seq santa-directions-seq)))))

;; Outputs
(defn level-task
  []
  (println "-- Level3 --")
  (println (str "Part1: " (count (:all-locations board-track))))
  (println (str "Part2: " (count (set (concat (:all-locations board-track-santa-only) (:all-locations board-track-robot-only))))))
  (println)
  )
