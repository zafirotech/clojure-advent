(ns clojure-advent.level10-elves-look-and-say)

(defn look-and-say
  "generate Look and Say"
  [string]
  (let [matcher (re-matcher #"((\d)\2*)" string)]
    (loop [match (re-find matcher) result ""]
      (if-not match
        result
        (recur (re-find matcher) (str result (count (second match)) (nth match 2))))
      )
    )
  )

(defn apply-several-times
  "Apply look-and-say several times"
  [string n]
  (last (take (inc n) (iterate look-and-say string)))
  )

;; 40 times
(def part1 (apply-several-times "3113322113" 40))

;; 10 more
(def part2 (apply-several-times part1 10))

;; Outputs
(defn level-task
  []
  (println "-- Level10 --")
  (println (str "Part1: " (count part1)))
  (println (str "Part2: " (count part2)))
  )

(level-task)