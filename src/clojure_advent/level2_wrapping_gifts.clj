(ns clojure-advent.level2-wrapping-gifts
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def order-list-raw (str/split-lines (slurp (io/resource "level2-input.txt"))))

(def order-list-mapped (map (fn [order-raw]
                              (let [order-list (map #(Long. %) (str/split order-raw #"x"))]
                                {:l (first order-list) :w (second order-list) :h (last order-list) :sorted-value (sort order-list)}))
                            order-list-raw))

;; Area calculation
(defn slack-area
  "Calculates minimum area of the surface"
  [etr]
  (min (* (:l etr) (:w etr)) (* (:l etr) (:h etr)) (* (:w etr) (:h etr))))

(defn surface-area
  "Calculates surface area"
  [etr]
  (* 2 (+ (* (:l etr) (:w etr)) (* (:w etr) (:h etr)) (* (:h etr) (:l etr)))))

(defn area
  "Calculates total area"
  [etr]
  (+ (surface-area etr) (slack-area etr)))


;; Ribbon calculation
(defn ribbon-length
  "Calculates ribbon bow length"
  [etr]
  (* (:l etr) (:w etr) (:h etr)))

(defn ribbon-bow
  "Calculates ribbon length"
  [etr]
  (* 2 (+ (first (:sorted-value etr)) (second (:sorted-value etr)))))

(defn ribbon
  "Calculates total ribbon length"
  [etr]
  (+ (ribbon-length etr) (ribbon-bow etr)))


;; Outputs
(defn level-task
  []
  (println "-- Level2 --")
  (println (str "Part1: " (reduce #(+ %1 (area %2)) 0 order-list-mapped)))
  (println (str "Part2: " (reduce #(+ %1 (ribbon %2)) 0 order-list-mapped)))
  (println)
  )
