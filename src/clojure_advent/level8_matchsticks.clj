(ns clojure-advent.level8-matchsticks
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def special-chars [{:pattern #"\\\"|\\\\|^\"|\"$" :dec-size 1}
                    {:pattern #"\\x[a-fA-F0-9]{2}" :dec-size 3}])

(def special-replacement-chars [{:pattern #"\\" :replacement "\\\\\\\\"}
                                {:pattern #"\"" :replacement "\\\\\""}])

(def all-words (str/split-lines (slurp (io/resource "level8-input.txt"))))

(defn dec-size
  "Count how much to substract from the total for each string"
  [str special-chars]
  (reduce
    (fn [dec-count special-char]
      (+ dec-count (* (:dec-size special-char) (count (re-seq (:pattern special-char) str))))
      )
    0 special-chars)
  )

(defn encode-dif
  "encode str to include requirements for part 2"
  [word]
  (let [encoded (str "\"" (reduce #(str/replace %1 (:pattern %2) (:replacement %2)) word special-replacement-chars) "\"")]
    (- (count encoded) (count word))
    )
  )

;; Outputs
(defn level-task
  []
  (println "-- Level8 --")
  (println (str "Part1: " (reduce (fn [diff-count word] (+ diff-count (dec-size word special-chars))) 0 all-words)))
  (println (str "Part2: " (reduce + (map encode-dif all-words))))
  (println)
  )