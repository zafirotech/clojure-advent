(ns clojure-advent.level6-xmas-light-grid
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def all-instructions (str/split-lines (slurp (io/resource "level6-input.txt"))))

(def board (take 1000 (repeat (take 1000 (repeat 0)))))

(defn action-function-maker
  "Light function maker (toggle, turn of or turn on"
  [action-str]
  (case action-str
    "toggle" (fn [value] (+ 2 value))
    "turn on" (fn [value] (inc value))
    "turn off" (fn [value] (max 0 (dec value)))))

(defn convert-coordinate
  "Convert str based instruction into formal instrucion"
  [str-instruction]
  (let [match (re-matches #"(toggle|turn on|turn off)\s(\d+),(\d+)\sthrough\s(\d+),(\d+)" str-instruction)
        action-str (nth match 1)
        x-up (nth match 2)
        y-up (nth match 3)
        x-down (nth match 4)
        y-down (nth match 5)]
    {:x-up (Integer. x-up) :y-up (Integer. y-up) :x-down (Integer. x-down) :y-down (Integer. y-down) :func (action-function-maker action-str)}
    )
  )

(defn update-row
  " Apply coordinate-function to all values in the coordinate-interval (only check for Xs)"
  [row coordinate]
  (map-indexed (fn
                 [index item]
                 (if (and (>= index (:x-up coordinate)) (<= index (:x-down coordinate)))
                   ((:func coordinate) item)
                   item)) row)
  )

(defn update-board
  " Apply coordinate-function to all values in the coordinate-interval (only check for Ys)"
  [board coordinate]
  (map-indexed (fn
                 [index item]
                 (if (and (>= index (:y-up coordinate)) (<= index (:y-down coordinate)))
                   (update-row item coordinate)
                   item)) board)
  )

(def total-lights-brightness
  (let
    [lit-board (reduce update-board board (map convert-coordinate all-instructions))]
    (reduce + (map (fn [row] (reduce + row)) lit-board))
    )
  )

;; Outputs
(defn level-task
  []
  (println "-- Level6 --")
  ;; I just refactored one to get to two. to see part 1 solution just look in the repository ;)
  (println (str "Part1: " "400410"))
  (println (str "Part2: " total-lights-brightness))
  (println)
  )

