(ns clojure-advent.level4-advent-coins
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(import 'java.security.MessageDigest
        'java.math.BigInteger)

(def secret-key "bgvyzdsv")

(defn md5 [s]
  (let [algorithm (MessageDigest/getInstance "MD5")
        size (* 2 (.getDigestLength algorithm))
        raw (.digest algorithm (.getBytes s))
        sig (.toString (BigInteger. 1 raw) 16)
        padding (apply str (repeat (- size (count sig)) "0"))]
    (str padding sig)))

(defn secret-number
  "Secret number mined in md5"
  [match-str]
  (loop [index 1]
    (let [md5sum (md5 (str secret-key index))]
      (if (str/starts-with? md5sum match-str)
        index
        (recur (inc index))
        )
      ))
  )


;; Outputs
(defn level-task
  []
  (println "-- Level4 --")
  (println (str "Part1: " (secret-number "00000")))
  (println (str "Part2: " (secret-number "000000")))
  (println)
  )
