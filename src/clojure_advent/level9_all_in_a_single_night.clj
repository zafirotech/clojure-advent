(ns clojure-advent.level9-all-in-a-single-night
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.math.combinatorics :as combo]))

(def all-distances (str/split-lines (slurp (io/resource "level9-input.txt"))))

(def distance-map
  (reduce
    (fn [dist-map dist-str]
      (let [match (re-matches #"^(\w+)\sto\s(\w+)\s=\s(\d+)$" dist-str)
            location1 (nth match 1)
            location2 (nth match 2)
            distance (Integer. (nth match 3))]
        (assoc dist-map {:location1 location1 :location2 location2} distance)
        )
      )
    {} all-distances
    )
  )

(defn get-distance
  "Find distance between two locations"
  [location1 location2]
  (get distance-map {:location1 location1 :location2 location2} (get distance-map {:location1 location2 :location2 location1}))
  )

(defn get-route-distance
  "Find distance in a path"
  [path]
  (reduce + (map #(get-distance (nth path (dec %)) (nth path %)) (range 1 (count path))))
  )

(def routes
  (let [locations (reduce (fn [locs key] (conj locs (:location1 key) (:location2 key))) #{} (keys distance-map))
        permutations (combo/permutations locations)]
    (map (fn [path] {:path path :distance (get-route-distance path)}) permutations)
    )
  )


(def short-route (reduce
                   (fn [short-route route]
                     (if (< (:distance short-route) (:distance route))
                       short-route
                       route)
                     )
                   (first routes) routes))

(def long-route (reduce
                  (fn [short-route route]
                    (if (> (:distance short-route) (:distance route))
                      short-route
                      route)
                    )
                  (first routes) routes))

;; Outputs
(defn level-task
  []
  (println "-- Level9 --")
  (println (str "Part1: " short-route))
  (println (str "Part2: " long-route))
  )