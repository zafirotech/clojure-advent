(ns clojure-advent.core
  (:require [clojure-advent.level1-elevator :as lvl1]
            [clojure-advent.level2-wrapping-gifts :as lvl2]
            [clojure-advent.level3-xmas-gifts :as lvl3]
            [clojure-advent.level4-advent-coins :as lvl4]
            [clojure-advent.level5-nice-and-nasty-words :as lvl5]
            [clojure-advent.level6-xmas-light-grid :as lvl6]
            [clojure-advent.level7-bobby-table-wires :as lvl7]
            [clojure-advent.level8-matchsticks :as lvl8]
            [clojure-advent.level9-all-in-a-single-night :as lvl9]
            [clojure-advent.level10-elves-look-and-say :as lvl10]
            [clojure-advent.level11_corporate_policy :as lvl11]
            )
  (:gen-class))

(defn exec-level
  "Exec level if valid argument"
  [lvl-function number-arg main-arg]
  (if (or (= "a" main-arg) (= number-arg main-arg))
    (lvl-function)
    )
  )

(defn -main
  "Main entry point"
  [& args]
  (let [arg (first args)]
    (exec-level lvl1/level-task "1" arg)
    (exec-level lvl2/level-task "2" arg)
    (exec-level lvl3/level-task "3" arg)
    (exec-level lvl4/level-task "4" arg)
    (exec-level lvl5/level-task "5" arg)
    (exec-level lvl6/level-task "6" arg)
    (exec-level lvl7/level-task "7" arg)
    (exec-level lvl8/level-task "8" arg)
    (exec-level lvl9/level-task "9" arg)
    (exec-level lvl10/level-task "10" arg)
    (exec-level lvl11/level-task "11" arg)
    )
  )

