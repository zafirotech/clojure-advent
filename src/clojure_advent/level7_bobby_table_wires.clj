(ns clojure-advent.level7-bobby-table-wires
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.math.numeric-tower :as math])
  )

(def table-map
  (let [all-instructions (str/split-lines (slurp (io/resource "level7-input.txt")))]
    (reduce (fn [map instruction]
              (let [match (re-matches #"(.*)\s->\s(\w+)" instruction)
                    gate (nth match 1)
                    wire-name (nth match 2)]
                (assoc map wire-name gate)
                )
              )
            {} all-instructions)
    )
  )

(defn decimal-to-binary
  "Convert decimal to binary"
  [x]
  (let [base-seq (map #(if (= %1 \1) 1 0) (seq (Integer/toString x 2)))]
    (concat (take (- 16 (count base-seq)) (repeat 0)) base-seq)
    )
  )

(defn binary-to-decimal
  "Convert binary to decimal"
  [bin-seq]
  (let [reduced-map (reduce
                      (fn
                        [conv-map bin-value]
                        {:value (+ (:value conv-map) (* bin-value (math/expt 2 (:pos conv-map))))
                         :pos   (dec (:pos conv-map))}
                        )
                      {:pos 15 :value 0} bin-seq)]
    (:value reduced-map)
    )
  )

(defn advn-and
  "our own base 16 AND"
  [x y]
  (let [bin-x (decimal-to-binary x)
        bin-y (decimal-to-binary y)
        bin-and (map #(* %1 %2) bin-x bin-y)]
    (binary-to-decimal bin-and)
    ))

(defn advn-or
  "our own base 16 OR"
  [x y]
  (let [bin-x (decimal-to-binary x)
        bin-y (decimal-to-binary y)
        bin-or (map #(bit-or %1 %2) bin-x bin-y)]
    (binary-to-decimal bin-or)
    ))

(defn advn-not
  "our own base 16 NOT"
  [x]
  (let [bin-x (decimal-to-binary x)
        bin-not (map #(if (= %1 1) 0 1) bin-x)]
    (binary-to-decimal bin-not)
    ))

(defn advn-rshift
  "our own base 16 RSHIFT"
  [x n]
  (let [bin-x (decimal-to-binary x)
        padding (take n (repeat 0))
        shifted (take (- (count bin-x) n) bin-x)
        bin-rshift (concat padding shifted)]
    (binary-to-decimal bin-rshift)
    ))

(defn advn-lshift
  "our own base 16 LSHIFT"
  [x n]
  (let [bin-x (decimal-to-binary x)
        padding (take n (repeat 0))
        shifted (take-last (- (count bin-x) n) bin-x)
        bin-rshift (concat shifted padding)]
    (binary-to-decimal bin-rshift)
    ))

(defn includes?
  "We need to invert the param order for str/includes?"
  [substr s]
  (str/includes? s substr))

(defn look-for-value
  "Look recursive for the value"
  [wire-name found-map table-map]
  (if (re-matches #"\d+" wire-name)
    {:value (Long/parseLong wire-name) :found-map found-map}
    (if (get found-map wire-name)
      {:value (get found-map wire-name) :found-map found-map}
      (let [new-entry (let [gate (get table-map wire-name)]
                        (condp includes? gate
                          "AND" (let [match (re-matches #"(\w+)\s+AND\s+(\w+)" gate)
                                      left-str (nth match 1)
                                      right-str (nth match 2)
                                      left-entry (look-for-value left-str found-map table-map)
                                      right-entry (look-for-value right-str (:found-map left-entry) table-map)
                                      and-value (advn-and (:value left-entry) (:value right-entry))
                                      ]
                                  {:value and-value :found-map (:found-map right-entry)}
                                  )
                          "OR" (let [match (re-matches #"(\w+)\s+OR\s+(\w+)" gate)
                                     left-str (nth match 1)
                                     right-str (nth match 2)
                                     left-entry (look-for-value left-str found-map table-map)
                                     right-entry (look-for-value right-str (:found-map left-entry) table-map)
                                     or-value (advn-or (:value left-entry) (:value right-entry))
                                     ]
                                 {:value or-value :found-map (merge (:found-map right-entry))}
                                 )
                          "NOT" (let [match (re-matches #"NOT\s+(\w+)" gate)
                                      str (nth match 1)
                                      entry (look-for-value str found-map table-map)
                                      not-value (advn-not (:value entry))
                                      ]
                                  {:value not-value :found-map (:found-map entry)}
                                  )
                          "RSHIFT" (let [match (re-matches #"(\w+)\s+RSHIFT\s+(\d+)" gate)
                                         left-str (nth match 1)
                                         right-str (nth match 2)
                                         left-entry (look-for-value left-str found-map table-map)
                                         right-value (Long/parseLong right-str)
                                         rshift-value (advn-rshift (:value left-entry) right-value)
                                         ]
                                     {:value rshift-value :found-map (:found-map left-entry)}
                                     )
                          "LSHIFT" (let [match (re-matches #"(\w+)\s+LSHIFT\s+(\d+)" gate)
                                         left-str (nth match 1)
                                         right-str (nth match 2)
                                         left-entry (look-for-value left-str found-map table-map)
                                         right-value (Long/parseLong right-str)
                                         lshift-value (advn-lshift (:value left-entry) right-value)
                                         ]
                                     {:value lshift-value :found-map (:found-map left-entry)}
                                     )
                          (look-for-value gate found-map table-map)
                          )
                        )
            ]
        {:value (:value new-entry) :found-map (assoc (:found-map new-entry) wire-name (:value new-entry))}
        )
      )
    )

  )

;; Outputs
(defn level-task
  []
  (println "-- Level 7 --")
  (let [part1-value (:value (look-for-value "a" {} table-map))
        updated-table-map (assoc table-map "b" (str part1-value))
        part2-value (:value (look-for-value "a" {} updated-table-map))]
    (do
      (println (str "Part1: " part1-value))
      (println (str "Part2: " part2-value))
      )
    )
  (println)

  )


