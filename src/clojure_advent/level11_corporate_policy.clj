(ns clojure-advent.level11_corporate_policy
  (:require [clojure.string :as str]))

(defn inc-char
  "increment char (cycle way). Only works for lower cases"
  [c]
  (if (= c \z)
    \a
    (let [cint (int c)
          next (inc cint)]
      (char next))
    )
  )

(defn inc-word
  "Increment word according to policy"
  [word]
  (let [word-array (vec (char-array word))
        inc-array (loop [remain word-array inc-array []]
                    (cond
                      (empty? remain) inc-array
                      (not (or (empty? inc-array) (= \a (first inc-array)))) (vec (concat remain inc-array))
                      :else (recur (subvec remain 0 (dec (count remain))) (vec (concat [(inc-char (last remain))] inc-array)))
                      ))
        ]
    (str/join inc-array)
    )
  )

(defn has-inc-straight?
  "Evaluate to true only if word contains at least 3 straight letters (Ex: abc, efg, etc.) "
  [word]
  (= 3 (:hold-diff (reduce (fn [current new-int]
                             (if (= (:hold-diff current) 3)
                               current
                               (if (= (inc (:hold-int current)) new-int)
                                 {:hold-diff (inc (:hold-diff current)) :hold-int new-int}
                                 {:hold-diff 1 :hold-int new-int}
                                 )
                               )
                             )
                           {:hold-diff 1 :hold-int (int \Z)}
                           (map int word)
                           )
         ))
  )

(defn has-mistakens?
  "Evaluate to true only if word contains 'i' or 'l'"
  [word]
  (or (str/includes? word "i") (str/includes? word "l"))
  )

(defn has-two-pairs?
  "Contains at least two different, non-overlapping pairs of letters"
  [word]
  (let [matcher (re-matcher #"([a-z])\1" word)
        matches (loop [match (re-find matcher) result #{}]
                  (if-not match
                    result
                    (recur (re-find matcher) (conj result (first match)))))]
    (>= (count matches) 2)
    )
  )



(defn next-valid-passwd
  "Returns next valid passwd sequence"
  [word]
  (loop [w word]
    (let [inc-w (inc-word w)]
      (if (and (has-inc-straight? inc-w) (not (has-mistakens? inc-w)) (has-two-pairs? inc-w))
        inc-w
        (recur inc-w)
        )
      )
    )
  )



;; Outputs
(defn level-task
  []
  (println "-- Level11 --")
  (println)
  (def inp "hepxcrrq")
  (println (str "Part1: " (next-valid-passwd inp)))
  (println (str "Part2: " (next-valid-passwd (next-valid-passwd inp))))
  )

(level-task)
