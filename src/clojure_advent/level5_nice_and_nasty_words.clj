(ns clojure-advent.level5-nice-and-nasty-words
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def all-words (str/split-lines (slurp (io/resource "level5-input.txt"))))

;; FIRST PART FUNCTIONS
(defn has-three-vowels?
  "To assert whether parameter contains 3 vowels or not"
  [word]
  (re-find #"[aeiou].*[aeiou].*[aeiou]" word))

(defn two-letter-in-a-row?
  "To assert whether there are two letters in a row"
  [word]
  (re-find #"([a-z])\1" word)
  )

(defn does-not-contain-ugly-strings?
  "To make sure there are not ugly strings inside"
  [word]
  (not (reduce #(or %1 (.contains word %2)) false ["ab" "cd" "pq" "xy"]))
  )

(defn is-nice?
  "whether is nice string or not"
  [word]
  (and (has-three-vowels? word) (two-letter-in-a-row? word) (does-not-contain-ugly-strings? word)))

;; SECOND PART FUNCTIONS
(defn two-letter-twice?
  "To assert whether there are two letters twice "
  [word]
  (re-find #"([a-z]{2}).*\1" word)
  )

(defn twice-same-with-in-between?
  "To assert whether there are two letters twice "
  [word]
  (re-find #"([a-z])[a-z]\1" word)
  )

(defn is-really-nice?
  "whether is 'really' nice string or not"
  [word]
  (and (two-letter-twice? word) (twice-same-with-in-between? word)))

;; General
(defn nice-calculator-maker
  "To reduce and add only nice ones"
  [nice-fn]
  (fn [count word]
    (if (nice-fn word) (inc count) count)))


;; Outputs
(defn level-task
  []
  (println "-- Level5 --")
  (println (str "Part1: " (reduce (nice-calculator-maker is-nice?) 0 all-words)))
  (println (str "Part2: " (reduce (nice-calculator-maker is-really-nice?) 0 all-words)))
  (println)
  )

